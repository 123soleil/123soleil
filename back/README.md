Freely inspired by the [RealWorld example app](https://github.com/gothinkster/node-express-realworld-example-app).

# back

> 123soleil backend

## Build Setup

``` bash
# install dependencies
npm install

# start server on port 3000
npm start
```

## Error Handling
In `routes/api/index.js`, an error-handling middleware for handling Mongoose's ValidationError is defined.
