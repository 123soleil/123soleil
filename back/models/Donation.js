var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var DonationSchema = new mongoose.Schema({
  amount: {
    type: Number,
    required: [true, "can't be blank"],
  },
  donor: {
    type: mongoose.Schema.Types.ObjectId, ref: 'User',
    required: [true, "can't be blank"],
  },
  receiver: {
    type: mongoose.Schema.Types.ObjectId, ref: 'Organization',
    required: [true, "can't be blank"],
  },
}, { timestamps: true });

DonationSchema.plugin(uniqueValidator, { message: 'is already taken.' });

DonationSchema.methods.toJSON = function() {
  return {
    id: this._id,
    amount: this.amount,
    receiver: this.receiver,
    date: this.createdAt,
  };
};

module.exports = mongoose.model('Donation', DonationSchema);
