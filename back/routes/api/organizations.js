var mongoose = require('mongoose');
var router = require('express').Router();
var Organization = require('../../models/Organization');
var Donation = require('../../models/Donation');

router.post('/organizations', (req, res, next) => {
  var org = new Organization();
  org.name = req.body.name;
  org.description = req.body.description;
  org.logo = req.body.logo;
  org.cause = req.body.cause;
  org.save().then(() => {
    return res.status(201).json({ organization: org.toJSON() });
  }).catch(next);
});

router.get('/organizations', (req, res, next) => {
  Organization.find().then((orgs) => {
    return res.json({ organizations: orgs.map(org => org.toJSON()) });
  }).catch(next);
});

router.get('/organizations/:id', (req, res, next) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.sendStatus(404);
  }
  Organization.findById(req.params.id).then((org) => {
    if (org) {
      return res.json({ organization: org.toJSON() });
    }
    return res.sendStatus(404);
  }).catch(next);
});

module.exports = router;
