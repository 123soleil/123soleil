var passport = require('passport');
var router = require('express').Router();
var User = require('../../models/User');

router.post('/users', (req, res, next) => {
  if (!req.body.password) {
    return res.status(422).json({errors: {password: "can't be blank"}});
  }
  var user = new User();
  user.firstname = req.body.firstname;
  user.lastname = req.body.lastname;
  user.email = req.body.email;
  user.adress = req.body.adress
  user.addressAddition = req.body.addressAddition
  user.zipCode = req.body.zipCode
  user.city = req.body.city
  user.country = req.body.country
  user.phone = req.body.phone
  user.setPassword(req.body.password);
  user.save().then(() => {
    return res.status(201).json({ user: user.toAuthJSON() });
  }).catch(next);
});

router.post('/users/login', (req, res, next) => {
  if (!req.body.email) {
    return res.status(422).json({errors: {email: "can't be blank"}});
  }
  if (!req.body.password) {
    return res.status(422).json({errors: {password: "can't be blank"}});
  }
  passport.authenticate('local', { session: false }, (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (user) {
      user.token = user.generateJWT();
      return res.json({user: user.toAuthJSON()});
    } else {
      return res.status(422).json(info);
    }
  })(req, res, next);
});

module.exports = router;
