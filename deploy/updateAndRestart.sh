#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/123soleil/123soleil/

# clone the repo again
git clone https://DEPLOY_USERNAME:DEPLOY_PWD@gitlab.com/123soleil/123soleil.git

# stop the previous pm2
pm2 kill

# starting pm2 daemon
pm2 status

cd /home/123soleil/123soleil/back

#install npm packages
echo "Running npm install"
npm install

#Restart the node server
pm2 start app.js --name 123soleil
