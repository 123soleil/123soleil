#!/bin/bash

# any future command that fails will exit the script
set -e
# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# ** Alternative approach
# echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
# chmod 600 /root/.ssh/id_rsa
# ** End of alternative approach

# disable the host key checking.
./deploy/disableHostKeyChecking.sh

# we have already setup the DEPLOYER_SERVER in our gitlab settings
DEPLOY_SERVER=$DEPLOY_SERVER

#replace DEPLOY_USERNAME AND DEPLOY_PWD with their values
sed -i -e "s/DEPLOY_USERNAME/$DEPLOY_USERNAME/g; s/DEPLOY_PWD/$DEPLOY_PWD/g" ./deploy/updateAndRestart.sh


# Lets ssh into the EC2 instance
# Once inside.
# 1. Stop the server
# 2. Take a pull
# 3. Start the server
echo "deploying to $DEPLOY_SERVER"
ssh 123soleil@$DEPLOY_SERVER 'bash -s' < ./deploy/updateAndRestart.sh
