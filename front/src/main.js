// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'

// Fontawesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(fas)
Vue.component('font-awesome-icon', FontAwesomeIcon)

// Global
require('./assets/scss/main.scss')

Vue.config.productionTip = false

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    currentDonations: [],
    user: {
      jwt: null
    }
  },
  mutations: {
    setDonation (state, newDonation) {
      let donationIdx = state.currentDonations.findIndex(donation => donation.organization.id === newDonation.organization.id)
      if (donationIdx >= 0) {
        state.currentDonations[donationIdx].amount = newDonation.amount
      } else {
        state.currentDonations.push({
          amount: newDonation.amount,
          organization: newDonation.organization
        })
      }
    },
    emptyDonations (state) {
      state.currentDonations = []
    },
    setUserToken (state, jwt) {
      state.user.jwt = jwt
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
