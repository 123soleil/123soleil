const BACKEND_URL = process.env.NODE_ENV === 'production'
  ? 'http://ec2-35-180-190-50.eu-west-3.compute.amazonaws.com:3000/api/v1/'
  : 'http://localhost:3000/api/v1/'

export default {
  backend_url: BACKEND_URL
}
